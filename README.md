
# AWS Lambda Function
> This project hosts an example lambda function that is packaged within gitlab CI/CD pipeline and pushed to AWS


## Overview
---
> The Lambda function is defined via `lambda_function.py`. 
> The packages required are defined in `requirements.txt`.
Steps:
- ZIP required libraries and lambda_function.py into a compressed archive
- Upload compressed archive to amazon s3
- update AWS Lambda function with object in amazon s3


## CI/CD
---
- `.gitlab-ci.yml` : Configured to package the lambda function within `example_with_dependencies`
> Variables can be configured to point at specific s3 bucket and update to lambda function
```
variables:
  S3_BUCKET_NAME: "mylambdabuckettest"
  LAMBDA_FN_NAME: "example_with_dependencies"
  LAMBDA_S3_DIR: "test/my-deployment-package.zip"
```

- `aws cli` : AWS credentials are placed within this repository's CI/CD variables
    - during runtime gitlab pipelines can access these variables as environment variables

![AWS_CREDS](./readme_images/image.png)
