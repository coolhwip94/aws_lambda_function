FROM ubuntu:21.04

RUN apt update -y
RUN apt upgrade -y
RUN DEBIAN_FRONTEND=noninteractive apt install python3-pip -y
RUN apt install awscli -y
RUN apt install zip -y

CMD [ "bash" ]